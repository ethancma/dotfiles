if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Path to your oh-my-zsh installation.
export ZSH="/Users/ethanma/.oh-my-zsh"

# -- Theme
ZSH_THEME="powerlevel10k/powerlevel10k"

# -- Plugins
plugins=(
git
zsh-syntax-highlighting
zsh-autosuggestions
history
colored-man-pages
web-search
)

# Automatically update oh-my-zsh
export UPDATE_ZSH_DAYS=14

source $ZSH/oh-my-zsh.sh

bindkey '^F' autosuggest-accept

# -- Aliases
# Shell
alias la="ls -a"
alias c=clear
alias desk="cd ~/Desktop"

# System
alias finder="open -a Finder"
alias safari="open -a Safari"
alias start="
open -a Safari
open -a Spotify
open -a Mail
open -a Discord"

# Config
alias zshconfig="vim ~/.zshrc"
alias vimconfig="vim ~/.vimrc"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
